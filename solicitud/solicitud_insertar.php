<?php
require('../config/session.php');
require('../config/control_acceso1.php');
require('../config/cabecera.php');
require('../config/menu.php');
require('../config/conexion.php');
?>
	<?php
	date_default_timezone_set('America/Caracas');
		?>
<script type="text/javascript" src="script.js"></script>
<script language=JavaScript>            
           function duplicatabla()
            {
                var tabla=document.getElementById("tabla1");
                var nuevatabla=tabla.cloneNode(true);
                tabla.parentNode.appendChild(nuevatabla);
            }
            function borratabla()
            {
                tablas=document.getElementsByTagName("table")
                ultima=tablas.length-1;
                tablas[ultima].parentNode.removeChild(tablas[ultima]);
            }              
        </script>
        <form action="solicitud_guardar.php" method="POST" name="formulario" onsubmit="return validar()" autocomplete="off">
       
		<div class="box col-lg-6 ">
                <div class="box-header">
                  <h3 class="box-title">Procesar Solicitud</h3>
                </div><!-- /.box-header -->
                <div class="box box-primary">
		<div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tbody>
			<tr><td>Codigo:</td><td><div class="col-xs-5"><input type="text" class="form-control input-sm" name="cod_solicitud" required ></div></td></tr>
                    <tr><td>Fecha:</td><td><div class="col-xs-5"><input type="text" value="<?=date('Y/m/d')?>" class="form-control input-sm" name="fecha_solicitud" required></div></td>
		    <td>Hora:</td><td><div class="col-xs-6"><input type="time" value="<?=date('H:m')?>" class="form-control timepicker" name="hora_solicitud" required></div></td></tr>
                    <tr>
		    <td>Solicitante:</td><td><div class="col-xs-5"><input type="text" class="form-control input-sm" name="cod_solicitante" id="cod_solicitante" onkeyup="autocomplet()" required >
		    <ul id="solicitante_lista_cod" style="list-style: none; margin-left: 0px; padding-left: 0px; padding-top: 10px;"></ul>
		    </div></td></tr>
                  </tbody>
		  </table>
                </div><!-- /.box-body -->
		</div>
              </div>
		
		<div class="box col-lg-12">
		<div class="box-body table-responsive no-padding">
                  <table  class="table table-hover">
                    <tbody>
                    <tr><th>Codigo Sap</th><th>Nombre</th><td></td><th>Cant. Disponible</th><th>Cant. Solicitada</th><th>Observación</th></tr>
		    </tbody>
		  </table>
		  <table id="tabla1" class="table table-hover">
                    <tbody>
		    <tr>
		    <td><input type="text" class="form-control input-sm" name="cod_sap[]" onblur="cargaDatos(this)" required></td>
		    <td><input type="text" class="form-control input-sm" disabled></td>
		    <td><input type="text" class="form-control input-sm" disabled></td>
		    <td><input type="number" class="form-control input-sm cantidad-entrega" name="cant_entrega[]" min="1" required></td>
		    <td><textarea class="form-control" rows="2" name="observacion[]"></textarea></td>
		    </tr>
                  </tbody>
		  </table>
                </div><!-- /.box-body -->
		</div>
		
            <div class="col-xs-5"></div><button type="submit" class="btn btn-primary">Procesar</button><a href="../usuario/home.php" type="submit" class="btn btn-danger">Volver  </a>
        </form>
	<button onclick="duplicatabla()";>Añadir Campo</button>
            <button onclick="borratabla()";>Borrar Campo</button>
		</div>
	    </div>
	<script>
		function cargaDatos(cod_sap) {
			$.ajax({
				method: "POST",
				url: "buscar_inventario.php",
				data: { cod_sap: cod_sap.value }
			      }).done(function(msg) {
				cod_sap.parentNode.parentNode.childNodes[3].firstChild.value = JSON.parse(msg)['nomb_material'];
				cod_sap.parentNode.parentNode.childNodes[5].firstChild.value = JSON.parse(msg)['cant_material'];
				cod_sap.parentNode.parentNode.childNodes[7].firstChild.setAttribute("max", JSON.parse(msg)['cant_material'] );
			});
		}
	</script>
        <?php
    require("../config/pie_pagina.php");
    ?>
