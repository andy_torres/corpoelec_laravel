<?php
require('../config/session.php');
require('../config/cabecera.php');
require('../config/menu.php');
require('../config/conexion.php');

$solicitantes = "SELECT * from solicitante order by proce_solicitante ASC";
$resultado = mysql_query($solicitantes, $conexion);
?>
       
		<div class="box col-lg-12">
                <div class="box-header">
                  <h3 class="box-title">Solicitantes</h3>
                </div><!-- /.box-header -->
                <div class="box box-primary">
						<div class="box-body">
								<table id="example1" class="table table-bordered table-hover">
										<thead>
												<tr>
												<th>Cédula</th>
												<th>Nombre</th>
												<th>Procedencia</th>
												<th>Teléfono</th>
												<th>Acciones</th>
												
												</tr>
										</thead>
										
										<tbody>
										<?php while($fila = mysql_fetch_array($resultado)):?>
												<tr>
														<td><?=$fila['cedu_solicitante']?></td>
														<td><?=$fila['nomb_solicitante']?></td>
														<td><?=$fila['proce_solicitante']?></td>
														<td><?=$fila['telef_solicitante']?></td>
														<td>
																<a class="btn btn-primary btn-sm" href="solicitante_editar.php?codigo=<?=$fila['cod_solicitante']?>" title="Modificar"><i class="fa fa-pencil"></i></a>
														</td>
												      
												</tr>
												<div id="myModal" class="modal fade" role="dialog">
														<div class="modal-dialog">
													  
														  <!-- Modal content-->
														  <div class="modal-content">
															<div class="modal-header">
															  <button type="button" class="close" data-dismiss="modal">&times;</button>
															  <h4 class="modal-title">¡CUIDADO!</h4>
															</div>
															<div class="modal-body">
															  <p>Se eliminará este registro de forma permanente.<br>si está seguro presione "eliminar", de lo contario presione "cancelar"</p>
															</div>
															<div class="modal-footer">
															  <a class="btn btn-danger" href="solicitante_fun_eliminar.php?codigo=<?=$fila['cod_solicitante']?>" >eliminar</a>
															  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
															</div>
														  </div>
													  
														</div>
														
														
										</div>	
										<?php endwhile;?>
										<!-- Modal -->

										</tbody>
										
								</table>
								<hr>
								<a href="solicitante_insertar.php" class="btn btn-primary">Ingresar Solicitante</a>
						</div><!-- /.box-body -->
				</div>
        </div>
		</div>
	    </div>
	<!-- Trigger the modal with a button -->



		
        <?php
   
    
    require("../config/pie_pagina.php");
    ?>
	
