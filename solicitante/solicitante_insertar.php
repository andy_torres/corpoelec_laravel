<?php
require('../config/session.php');
require('../config/control_acceso1.php');
require('../config/cabecera.php');
require('../config/menu.php');
require('../config/conexion.php');
?>


        <form action="solicitante_fun.php" method="POST">
       
		<div class="box col-lg-6 ">
                <div class="box-header">
                  <h3 class="box-title">Procesar Solicitante</h3>
                </div><!-- /.box-header -->
                <div class="box box-primary">
		<div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tbody>
  <tr><td>Cédula</td><td><div class="col-xs-6"><input type="text" class="form-control input-sm" required placeholder="00000000" title="Formato para la cedula debe ser XXXXXXXX" name="cedu_solicitante"></div></td>
		    <td>Nombre:</td><td><div class="col-xs-6"><input type="text" class="form-control" required name="nomb_solicitante" title="Coloque primer nombre y primer apellido. Ejemplo Pedro Perez"></div></td></tr>
              <tr><td>Teléfono</td><td><div class="col-xs-6"><input type="text" class="form-control input-sm" name="telef_solicitante"></div></td>
		    <td>Dirección:</td><td><div class="col-xs-6"><input type="text" class="form-control" required name="direc_solicitante" title="Debe escribir la localidad donde reside el solicitante ejemplo: Mérida,Av las americas " ></div></td></tr>
			  
			        <tr>
					<td>Procedencia:</td>
					<td><div class="col-xs-8">
                    <input type= "text" class="form-control" name="proce_solicitante" title="debe espesificar de que oficina o departamento proviene el solicitante. " >
					</div></td>
		    </tr>
                  </tbody>
		  </table>
                </div><!-- /.box-body -->
		</div>
              </div>
		<div class="row">
			<div class="col-lg-8 col-lg-offset-5"><button type="submit" class="btn btn-primary">Guardar</button> <button type="reset" class="btn btn-danger">Borrar</button> </div>	
		</div>
            
			
        </form>
	
		</div>
	    </div>
        <?php
   
    
    require("../config/pie_pagina.php");
    ?>
