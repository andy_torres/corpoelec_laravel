<?php
    require('../config/session.php');
    require('../config/cabecera.php');
    require('../config/menu.php');
    require('../config/conexion.php');
?>
    <?php
    date_default_timezone_set('America/Caracas');
        ?>
<script type="text/javascript" src="script.js"></script>
<h3>Consultar Incorporaciones de Material Realizadas</h3><br>
<form action="consulta_carga_fun.php" method="POST" autocomplete="off">
     <div class="col-xs-12">  
       <div class="row">
        
                <div class="col-md-2"></div>
        <div class="col-md-4">
        <div class="info-box ">
                <div class="info-box-content">
                  <input type="radio" name="tipo_consulta"value="2"
                        onclick="cod_ingreso.disabled=false;
             fecha_inicio.disabled=true;fecha_fin.disabled=true">Codigo de Entrega
                       <input type="text" class="form-control input-sm" name="cod_ingreso" required >
                </div><!-- /.info-box-content -->
              </div>
       </div>
        
            
      
        
        <div class="col-md-4">
        <div class="info-box bg blue">
                <div class="info-box-content">
                  <input type="radio" name="tipo_consulta"value="4"
                        onclick="cod_ingreso.disabled=true;
            fecha_inicio.disabled=false;fecha_fin.disabled=false">Rango de Fecha<br>
                       Desde:<input type="date" class="form-control input-sm" name="fecha_inicio" required>
            Hasta:<input type="date"  class="form-control input-sm" name="fecha_fin" required>
                </div><!-- /.info-box-content -->
              </div>
       </div>
        
        
        
       </div>
            <div class="col-xs-5"></div><button type="submit" class="btn btn-primary">Consultar</button><a href="../usuario/home.php" type="submit" class="btn btn-danger">Volver  </a>
     </div>
        </form>
<?php
   require('../config/pie_pagina.php');
?>