    <?php
    require('../config/session.php');
    require('../config/cabecera.php');
    require('../config/menu.php');
    require('../config/conexion.php');
    ?>
     
    <script type="text/javascript" src="script.js"></script>
        <form action="materiales_lista.php" method="POST" >
	    <div class="col-xs-9">
	    <div class="box col-lg-6 col-lg-offset-2">
                <div class="box-header ">
                  <h3 class="box-title">Consultar Material</h3>
                </div><!-- /.box-header -->
                <div class="box box-primary ">
		<div class="box-body table-responsive no-padding ">
                  <table class="table table-hover">
                    <tbody>
                    <tr><div class="col-xs-3"><td>Nombre de Material:</td></div><div class="col-xs-3"><td><input type="text" class="form-control input-sm" name="cod_sap" id="cod_sap" onkeyup="autocomplet()">
		    <ul id="materiales_lista_cod"></ul></td></div></tr>
                  </tbody></table>
		  
                </div><!-- /.box-body -->
		</div>
              </div>
            <div class="col-xs-7"></div><button type="submit" class="btn btn-primary">Consultar</button>
		</div>
        </form>
    
    <?php
    require('../config/pie_pagina.php');
    ?>
