<?php
require('../config/session.php');
require('../config/control_acceso1.php');
require('../config/cabecera.php');
require('../config/menu.php');
require('../config/conexion.php');


?>
        <form action="" method="POST">
	    <div class="col-xs-9">
		<div class="box col-lg-6 col-lg-offset-2">
                <div class="box-header">
                  <h3 class="box-title">Ingresar Nuevo Material al Inventario</h3>
                </div><!-- /.box-header -->
                <div class="box box-primary">
		<div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tbody>
                    <tr><td>Codigo Sap:</td><td><div class="col-xs-5"><input type="text" class="form-control input-sm" name="cod_sap"></div></td></tr>
                    <tr><td>Nombre:</td><td><div class="col-xs-5"><input type="text" class="form-control input-sm" name="nomb_material"></div></td></tr>
		    <tr><td>Tipo:</td><td><div class="col-xs-5"><input type="text" class="form-control input-sm" name="tipo_material"></div></td></tr>
		    <tr><td>¿Contador?</td><td><input type="checkbox" class="minimal"value="1" name="checkbox"></td></tr>
		    <tr><td>Marca:</td><td><div class="col-xs-5"><input type="text" class="form-control input-sm" name="marca_material"></div></td></tr>
		    <tr><td>Descripcion:</td><td><div class="col-xs-5"><textarea class="form-control" rows="3" name="desc_material"></textarea></div></td></tr>
		    <tr><td>Unidad:</td><td><div class="col-xs-5"><input type="text" class="form-control input-sm" name="unid_material"></div></td></tr>
		    <tr><td>Cantidad:</td><td><div class="col-xs-5"><input type="text" class="form-control input-sm" name="cant_material"></div></td></tr>
                  </tbody>
		  </table>
                </div><!-- /.box-body -->
		</div>
		            <div class="col-xs-5"></div><button type="submit" class="btn btn-primary">Guardar</button><a class="btn btn-danger " href="../index.php">Volver</a>

              </div>	   

	    
	    </div>
        </form>
		</div>
	    </div>
        <?php
    if ($_POST)
    {
		extract($_POST);
		$inserta="INSERT INTO materiales (cod_sap, nomb_material, tipo_material, checkbox, marca_material, desc_material, unid_material, cant_material )
                VALUES ('$cod_sap','$nomb_material', '$tipo_material','$checkbox','$marca_material', '$desc_material', '$unid_material', '$cant_material');";
		$resultado = mysql_query ($inserta, $conexion);
    if (!$resultado)
    { echo "<script>alert ('Fallo el registro del material')</script>";}
    else
    {echo "<script> alert('El registro ha sido Exitoso')</script>";}
    }
    
    require("../config/pie_pagina.php");
    ?>
