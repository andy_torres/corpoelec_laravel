<?php
    require('../config/session.php');
    require('../config/control_acceso1.php');
    require('../config/cabecera.php');
    require('../config/menu.php');
    require('../config/conexion.php');
?>
<?php
	date_default_timezone_set('America/Caracas');
		?>
<script language=JavaScript>            
           function duplicatabla()
            {
                var tabla=document.getElementById("tabla1");
                var nuevatabla=tabla.cloneNode(true);
                tabla.parentNode.appendChild(nuevatabla);
		
            }
            function borratabla()
            {
                tablas=document.getElementsByTagName("table")
                ultima=tablas.length-1;
                tablas[ultima].parentNode.removeChild(tablas[ultima]);
            }              
        </script>
        <form id="form" action="materiales_update.php" method="POST">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Cargar Material al Inventario</h3>
		  
                </div><!-- /.box-header -->
                <div class="box box-primary">
		<div class="box-body table-responsive no-padding">
                  <table id="tabla"class="table table-hover">
                    <tbody>
                    <tr>
		    <td>Codigo de Ingreso:</td>
		    <td><input type="text" class="form-control input-sm" name="cod_ingreso" required></td>
		    <td>Fecha:</td>
		    <td><input type="text" class="form-control input-sm" name="fecha_ingreso" value="<?=date('Y/m/d')?>" required></td>
		    
		    </tr>
		    </tbody>
		  </table>
		</div>
		</div>
	      </div>
	      <div class="box">
		<div class="box box-primary">
		<div class="box-body table-responsive no-padding">
		  <table id="tabla1"class="table table-hover">
                    <tbody>
                    <tr>
		    <td>Codigo Sap:</td>
		    <td><input type="text" class="form-control input-sm" name="cod_sap[]" onblur="cargaDatos(this)" required></td>
		    <td>Nombre:</td>
		    <td><input type="text" class="form-control input-sm" readonly></td>
		    <td>Cantidad:</td>
		    <td><input type="text" class="form-control input-sm" name="nueva_cant_material[]" required></td>
		    </tr>
                  
		    </tbody>
		  </table>
                </div><!-- /.box-body -->
		</div>
              </div>
	      
	      <div class="col-xs-5"></div><button type="submit" class="btn btn-primary">Cargar</button><a class="btn btn-danger " href="../index.php">Volver</a>
        </form>	
	<button onclick="duplicatabla()";>Añadir Campo</button>
            <button onclick="borratabla()";>Borrar Campo</button>
		</div>
	    </div>
        <script>
		function cargaDatos(cod_sap) {
			$.ajax({
				method: "POST",
				url: "buscar_inventario.php",
				data: { cod_sap: cod_sap.value }
			      }).done(function(msg) {
			    cod_sap.parentNode.parentNode.childNodes[7].firstChild.value = JSON.parse(msg)['nomb_material'];
			    
				
			});
		}
	</script>
    <?php
    require('../config/pie_pagina.php');
    ?>
