// autocomplet : this function will be executed every time we change the text
function autocomplet() {
	var min_length = 1; // 
	var keyword = $('#cod_sap').val();
	if (keyword.length >= min_length) {
		$.ajax({
			url: 'ajax_refresh.php',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
				$('#materiales_lista_cod').show();
				$('#materiales_lista_cod').html(data);
			}
		});
	} else {
		$('#materiales_lista_cod').hide();
	}
}


function set_item(item) {
	// change input value
	$('#cod_sap').val(item);
	// hide proposition list
	$('#materiales_lista_cod').hide();
}