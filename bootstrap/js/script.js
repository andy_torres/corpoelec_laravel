// autocomplet : this function will be executed every time we change the text
function autocomplet() {
	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#cod_solicitante').val();
	if (keyword.length >= min_length) {
		$.ajax({
			url: 'ajax_refresh.php',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
				$('#solicitante_lista_cod').show();
				$('#solicitante_lista_cod').html(data);
			}
		});
	} else {
		$('#solicitante_lista_cod').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_item(item) {
	// change input value
	$('#cod_solicitante').val(item);
	// hide proposition list
	$('#solicitante_lista_cod').hide();
}