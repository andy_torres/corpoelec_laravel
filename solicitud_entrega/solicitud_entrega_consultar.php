<?php
    require('../config/session.php');
    require('../config/cabecera.php');
    require('../config/menu.php');
    require('../config/conexion.php');
?>
	<?php
	date_default_timezone_set('America/Caracas');
		?>
<script type="text/javascript" src="script.js"></script>
<h3>Consultar Entregas Realizadas</h3><br>
<form action="solicitud_entrega_consulta_fun.php" method="POST" autocomplete="off">
	 <div class="col-xs-12">  
	   <div class="row">
	    
	    <div class="col-md-4">
	    <div class="info-box bg-grey">
                <div class="info-box-content">
                  <input type="radio" name="tipo_consulta" value="1"
                        onclick="serial_material.disabled=false; cod_solicitud.disabled=true;
			cod_solicitante.disabled=true; fecha_inicio.disabled=true;fecha_fin.disabled=true; cod_sap.disabled=true">Serial de Contador
                        <input type="text" class="form-control input-sm" name="serial_material" required>
                </div><!-- /.info-box-content -->
              </div>
	   </div>
	    
	    <div class="col-md-4">
	    <div class="info-box ">
                <div class="info-box-content">
                  <input type="radio" name="tipo_consulta"value="2"
                        onclick="serial_material.disabled=true; cod_solicitud.disabled=false;
			cod_solicitante.disabled=true; fecha_inicio.disabled=true;fecha_fin.disabled=true; cod_sap.disabled=true">Codigo de Solicitud
                       <input type="text" class="form-control input-sm" name="cod_solicitud" required >
                </div><!-- /.info-box-content -->
              </div>
	   </div>
	    
	    <div class="col-md-4">
	    <div class="info-box ">
                <div class="info-box-content">
                  <input type="radio" name="tipo_consulta"value="3"
                        onclick="serial_material.disabled=true; cod_solicitud.disabled=true;
			cod_solicitante.disabled=false; fecha_inicio.disabled=true;fecha_fin.disabled=true; cod_sap.disabled=true">solicitante
                        <input type="text" class="form-control input-sm" name="cod_solicitante" id="cod_solicitante" onkeyup="autocomplet()"  required>
			<ul id="solicitante_lista_cod" style="list-style: none; margin-left: 0px; padding-left: 0px; padding-top: 10px;"></ul>
                </div><!-- /.info-box-content -->
              </div>
	   </div>
	    
	   </div>
	   <div class="row">
	    
	    <div class="col-md-4">
	    <div class="info-box bg blue">
                <div class="info-box-content">
                  <input type="radio" name="tipo_consulta"value="4"
                        onclick="serial_material.disabled=true; cod_solicitud.disabled=true;
			cod_solicitante.disabled=true; fecha_inicio.disabled=false;fecha_fin.disabled=false; cod_sap.disabled=true">Rango de Fecha<br>
                       Desde:<input type="date"value="<?=date('d/m/Y')?>" class="form-control input-sm" name="fecha_inicio" required>
			Hasta:<input type="date" value="<?=date('d/m/Y')?>" class="form-control input-sm" name="fecha_fin" required>
                </div><!-- /.info-box-content -->
              </div>
	   </div>
	    
	    <div class="col-md-4">
	    <div class="info-box ">
                <div class="info-box-content">
                  <input type="radio" name="tipo_consulta"value="5"
                        onclick="serial_material.disabled=true; cod_solicitud.disabled=true;
			cod_solicitante.disabled=true; fecha_inicio.disabled=true;fecha_fin.disabled=true; cod_sap.disabled=false">Codigo SAP de Material</td>
                        <input type="text" class="form-control input-sm" name="cod_sap" required>
                </div><!-- /.info-box-content -->
              </div>
	   </div>
	    
	    <div class="col-md-4">
	    <div class="info-box ">
                <div class="info-box-content">
                  <input type="radio" class="form-group" name="tipo_consulta"value="6"
                        onclick="serial_material.disabled=true; cod_solicitud.disabled=true;
			cod_solicitante.disabled=true; fecha_inicio.disabled=true;fecha_fin.disabled=true; cod_sap.disabled=true">Consulta General de Inventario
                </div><!-- /.info-box-content -->
              </div>
	   </div>
	   </div>
            <div class="col-xs-5"></div><button type="submit" class="btn btn-primary">Consultar</button><a href="../usuario/home.php" type="submit" class="btn btn-danger">Volver  </a>
	 </div>
        </form>
<?php
   require('../config/pie_pagina.php');
?>
