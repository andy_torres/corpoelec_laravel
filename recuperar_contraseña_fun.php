<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Sistema de Inventario</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="dist/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="bootstrap/css/ionicons.min.css" rel="stylesheet" type="text/css" />    
    <!-- Theme style -->
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

    
  </head>
<?php
require('config/conexion.php');

$email=$_POST['email'];
$ced_usuario=$_POST['ced_usuario'];

$orden="SELECT * FROM usuario WHERE email='$email' and ced_usuario='$ced_usuario'";
$listado=mysql_query($orden);

if (mysql_num_rows($listado) > 0 )
{
  $lista=mysql_fetch_array($listado);
?>
<body>
  <div class="row"> <br></div>
  <div class="row">
	<div class="col-lg-8 col-lg-offset-4">
<IMG SRC="config/corpoelec.png" class="img-responsive"/>	</div>
  </div>
  <form action="recuperar_contraseña_validar.php" method="POST"autocomplete="off">
	<div class="row " style="padding-top: 150px">
	  <div class="col-lg-6 col-lg-offset-3">
		<h3 class="box-title text-center">Sistema Automatizado de Inventario<br> (Oficina de Servicio Técnico)<br> Corpoelec Mérida I</h3>
		<div class="box-header">
		  <h3 class="box-title">Recuperar Contraseña</h3>
        </div><!-- /.box-header -->
        <div class="box box-primary">
		  <div class="box-body table-responsive no-padding">
					<table class="table table-hover">
					 
<tbody>
                    <tr>
        <td>Correo Electronico:</td>
        <td><input type="email" class="form-control input-sm" name="email" value="<?=$lista['email']?>" readonly></td>         
          <td> Numero de Cedula</td>
          <td><input type="text" class="form-control input-sm" name="ced_usuario" value="<?=$lista['ced_usuario']?>" readonly></td>
        </tr> 
        <tr>
          <td>Pregunta de Seguridad:</td>
          <td colspan="3"><input type="text" class="form-control input-sm" name="pregunta_seguridad" value="<?=$lista['pregunta_seguridad']?>" ></td>  
        </tr>
        <tr>       
          <td>Ingrese su Respuesta de Seguridad</td>
          <td colspan="3"><input type="text" class="form-control input-sm" name="respuesta_seguridad" onkeyup = "this.value=this.value.toUpperCase()"></td>
        </tr>        
        </tbody>




            
					</table>
		  </div><!-- /.box-body -->
		</div>
		<div class="col-xs-12 text-center">
		  <input type="submit" href="home.php" class="btn btn-primary" value="Siguiente">
		</div>
      </div>
	</div>
  </form>
	    </div>

  <?php
}
else
       {        
            echo "<script type=\"text/javascript\"> alert('Este Usuario no esta Registrado'); window.location='recuperar_contraseña.php';</script>";
  }
  ?>
     
       <!-- jQuery 2.1.3 -->
    <script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- jQuery UI 1.11.2 -->
	<!--<script src="http://code.jquery.com/jquery-2.1.4.js" type="text/javascript"></script>-->
    <!--<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>-->
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!--<script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>-->
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
    
	<!-- DATA TABES SCRIPT -->
    <script src="plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
	
	<!-- Morris.js charts -->
    <script src="config/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js" type="text/javascript"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/knob/jquery.knob.js" type="text/javascript"></script>
    <!-- daterangepicker -->
    <script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <!-- datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js" type="text/javascript"></script>

    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--<script src="../dist/js/pages/dashboard.js" type="text/javascript"></script>-->

    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js" type="text/javascript"></script>
    <script type="../bootstrap/js/jquery.js"></script>
    <script type="text/javascript">
      $(function () {
        $("#example1").dataTable({
		  responsive: true,
		  "order": [[1, 'asc']],
		  "oLanguage": {
			"sUrl": "../plugins/datatables/datatable.spanish.txt"
		  },
                              "iDisplayLength": 5,
                              "aLengthMenu":[[5,10,15,20,-1],[5,10,15,20,"Todos"]],

		  });
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
      });
    </script>
	
	
  </body>
</html>
