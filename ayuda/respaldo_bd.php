<?php
require('../config/session.php');
require('../config/cabecera.php');
require('../config/menu.php');
require('../config/conexion.php');
?>
<div class='container'>
	<div class="row">
	<span><h4>Respaldo y Restauracion de Base de Datos del Sistema</h4></span>
      </div><br><br>
	<div class='well'>		
			
			<div class='row-fluid'>
				<div class='span12 text-center'>
				    <span><h4><p class="text-info">Respaldar Información</p></h4></span>
				</div>
			</div>
			<div class='row-fluid'>
				<div class="span12 text-center">
				    <span>Respaldar la información registrada en el Sistema</span>
				</div><br>
			</div>
			<div class="row-fluid">
				<div class="span12 text-center">
<input type=button value='Respaldar' class="btn btn-primary" TITLE='respalda base de datos del sistema' onClick=location.href='respaldar.php?act=respaldar'>
				</div>
			</div><br><br>
			<div class='row-fluid'>
				<div class='span12 text-center'>
				    <span><h4><p class="text-info">Restaurar Información</p></h4></span>
				</div>
			</div>
			<div class='row-fluid'>
				<div class="span12 text-center">
				    <span>Restaurar la información con un archivo de respaldo guardado</span>
				</div>
			</div>
			<form class="text-center" action='respaldar.php?act=aplicarresp' method='post' enctype='multipart/form-data'><br>
<center><input name='archivo' type='file' size='20'/></center><br>
<input name='enviar' type='submit' TITLE='restaura base de datos del sistema' value='Restaurar' class="btn btn-primary" />
<input name='action' type='hidden' value='upload' />     
			</form>
		</div>
	</div>
</div>
</div>
</div>
  </body>


<?php
    require("../config/pie_pagina.php");
?>