<?php
require('../config/session.php');
require('../config/cabecera.php');
require('../config/menu.php');
require('../config/conexion.php');
?>

        <!-- /.row -->
          <!-- Main row -->
<div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
              <!-- Custom tabs (Charts with tabs)-->
              <!-- TO DO List -->
              <h3>
            Inventario en Stock
            
                   </h3>
	      <div class="box box-primary">
                
                <div class="box-body">
                  <ul class="todo-list">
                    
                      
                    <li>
                      
                    </li>
                  </ul>
                  <div class="row">
		    
                    <div class="col-lg-12">
                      <?php
                      require('../config/conexion.php');
                       $materialesdisponibles= "SELECT * FROM `materiales` WHERE `cant_material` != 0";
                        $resultado= mysql_query($materialesdisponibles,$conexion);
                      ?>
                      <table id="example1" class="table table-bordered table-hover">
										<thead>
												<tr>
												<th>Nombre del Material</th>
												<th>Cantidad Disponible</th>
												<th>Tipo de Material</th>
												<th>Descripcion del material</th>
												
												
												</tr>
										</thead>
										
										<tbody>
										<?php while($fila = mysql_fetch_array($resultado)):?>
												<tr>
														<td><?=$fila['nomb_material']?></td>
														<td><?=$fila['cant_material']?></td>
														<td><?=$fila['tipo_material']?></td>
														<td><?=$fila['desc_material']?></td>
                                                </tr>
										<?php endwhile;?>
										<!-- Modal -->

										</tbody>
										
								</table>
                      
                    </div>
                  </div>
                  
                </div><!-- /.box-body -->
                
              </div><!-- /.box -->

              
            </section><!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
           
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
    </div><!-- ./wrapper -->

    <script type="text/javascript">
      $(function () {
        $("#example1").dataTable({
		  responsive: true,
		  "order": [[1, 'asc']],
		  "oLanguage": {
			"sUrl": "../plugins/datatables/datatable.spanish.txt"
		  },
                              "iDisplayLength": 5,
                              "aLengthMenu":[[5,10,15,20,-1],[5,10,15,20,"Todos"]],

		  });
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
      });
    </script>
	
	<script type="text/javascript">
    var idleTime = 0;
    $(document).ready(function () {
        //Increment the idle time counter every minute.
        var idleInterval = setInterval(timerIncrement, 1000); // 1 second

        //Zero the idle timer on mouse movement.
        $(this).mousemove(function (e) {
            idleTime = 0;
        });
        $(this).keypress(function (e) {
            idleTime = 0;
        });
    });

    function timerIncrement() {
        idleTime = idleTime + 1000;
        if (idleTime > 600000) { // 30 seconds
            location.href="cerrar.php";
        }
    }
    </script>

  </body>
</html>
<?php
   
    
    require("../config/pie_pagina.php");
    ?>