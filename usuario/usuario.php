<?php
require('../config/session.php');
require('../config/control_acceso.php');
//require('../config/control_acceso1.php');
require('../config/cabecera.php');
require('../config/menu.php');
require('../config/conexion.php');
?>

 
          
        <form action="usuario_fun.php" method="POST" autocomplete="off">
       
		<div class="box col-lg-6 ">
                <div class="box-header">
                  <h3 class="box-title">Crear Nuevo Usuario</h3>
                </div><!-- /.box-header -->
                <div class="box box-primary">
		<div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tbody>
				<tr><td>Nombre:</td><td><div class="col-xs-6"><input type="text" class="form-control input-sm" required maxlength="15" minlength="4" name="nomb_usuario"></div></td>
			    <td>Cedula:</td><td><div class="col-xs-6"><input type="text" class="form-control" required maxlength="8" required name="ced_usuario"pattern="[0-9]{7,9}"
title="La cedula debe tener este formato 0000000 y Debe contener al menos 7 Numeros"></div></td></tr>
                    <tr><td>Tipo:</td><td><div class="col-xs-6">
                    <select class="form-control" name="tipo_usuario">
                        <option value="Administrador">Administrador</option>
                        <option value="Usuario">Usuario</option>
                    </select></div></td>
			    <td>Correo:</td><td><div class="col-xs-6"><input type="email" class="form-control" required maxlength="30" required name="email" ></div></td></tr>
                <td>Contraseña:</td><td><div class="col-xs-6"><input type="password" class="form-control" required name="passw_usuario" ></div></td></tr>
                <tr><td>Pregunta de Seguridad</td><td><div class="col-xs-6"><input type="text" class="form-control" required name="pregunta_seguridad" onkeyup = "this.value=this.value.toUpperCase()"></td>
        <td>Respuesta:</td><td><div class="col-xs-6"><input type="text" class="form-control" required name="respuesta_seguridad" onkeyup = "this.value=this.value.toUpperCase()"></td>
                  </tbody>
		  </table>
                </div><!-- /.box-body -->
		</div>
              </div>
		
            <div class="col-xs-5"></div><input type="submit" href="index.php"  class="btn btn-primary" value="Guardar">
        </form>
	
		</div>
	    </div>
        <?php
   
    
    require("../config/pie_pagina.php");
    ?>
