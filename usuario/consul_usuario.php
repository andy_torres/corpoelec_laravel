<?php
require('../config/session.php');
require('../config/cabecera.php');
require('../config/menu.php');
require('../config/conexion.php');

$orden = "SELECT * from usuario order by nomb_usuario ASC";
$resultado = mysql_query($orden, $conexion);
?>
  	<?php
	date_default_timezone_set('America/Caracas');
		?>     
		<div class="box col-lg-12">
                <div class="box-header">
                  <h3 class="box-title">Usuarios</h3>
                </div><!-- /.box-header -->
                <div class="box box-primary">
						<div class="box-body">
								<table id="example1" class="table table-bordered table-hover">
										<thead>
												<tr>
												<th>Codigo</th>
												<th>Nombre</th>
												<th>Cedula</th>
											<th>Tipo de Usuario</th>
										<th>Correo Electronico</th>
										<th>Acciones</th>		
												</tr>
										</thead>
										
										<tbody>
										<?php while($fila = mysql_fetch_array($resultado)):?>
												<tr>
														<td><?=$fila['cod_usuario']?></td>
														<td><?=$fila['nomb_usuario']?></td>
														<td><?=$fila['ced_usuario']?></td>

<td><?=$fila['tipo_usuario']?></td>

<td><?=$fila['email']?></td>
																											<td>
<a class="btn btn-primary btn-sm"  href="usuario_editar.php?codigo=<?=$fila['cod_usuario']?>"><i class="fa fa-pencil"></i></a>

</td>

												</tr>

												<div id="myModal" class="modal fade" role="dialog">
														<div class="modal-dialog">
													  
														  <!-- Modal content-->
														  <div class="modal-content">
															<div class="modal-header">
															  <button type="button" class="close" data-dismiss="modal">&times;</button>
															  <h4 class="modal-title">¡CUIDADO!</h4>
															</div>
															<div class="modal-body">
															  <p>Se eliminará este registro de forma permanente.<br>si está seguro presione "eliminar", de lo contario presione "cancelar"</p>
															</div>
															<div class="modal-footer">
															  <a class="btn btn-danger" href="usuario_eliminar.php?codigo=<?=$fila['cod_usuario']?>" >eliminar</a>
															  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
															</div>
														  </div>
													  
														</div>
										<?php endwhile;?>
										</tbody>
										
								</table>
								<hr>
						</div><!-- /.box-body -->
				</div>
        </div>
		</div>
	    </div>
		
		
        <?php
   
    
    require("../config/pie_pagina.php");
    ?>
