-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 18-10-2016 a las 19:30:06
-- Versión del servidor: 10.0.23-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `inventario_corpoelec`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE `bitacora` (
  `id_bitacora` int(11) NOT NULL,
  `cod_usuario` int(11) NOT NULL,
  `accion_bitacora` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `tabla_bitacora` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_bitacora` datetime NOT NULL,
  `datos_bitacora` varchar(70) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materiales`
--

CREATE TABLE `materiales` (
  `cod_sap` int(10) NOT NULL,
  `nomb_material` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `tipo_material` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `desc_material` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `unid_material` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `cant_material` int(6) NOT NULL,
  `marca_material` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `checkbox` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Volcado de datos para la tabla `materiales`
--

INSERT INTO `materiales` (`cod_sap`, `nomb_material`, `tipo_material`, `desc_material`, `unid_material`, `cant_material`, `marca_material`, `checkbox`) VALUES
(112233, 'Contador', 'monofasico', 'contador monofasico 13-40', 'pza', 1612, 'janz', 1),
(332211, 'AD', 'sad', 'dfsdf', 'Pza', 1509, 'sffs', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitante`
--

CREATE TABLE `solicitante` (
  `cod_solicitante` int(11) NOT NULL,
  `cedu_solicitante` int(12) NOT NULL,
  `nomb_solicitante` varchar(20) COLLATE utf8_swedish_ci NOT NULL,
  `proce_solicitante` text COLLATE utf8_swedish_ci NOT NULL,
  `telef_solicitante` varchar(12) COLLATE utf8_swedish_ci NOT NULL,
  `direc_solicitante` varchar(20) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Volcado de datos para la tabla `solicitante`
--

INSERT INTO `solicitante` (`cod_solicitante`, `cedu_solicitante`, `nomb_solicitante`, `proce_solicitante`, `telef_solicitante`, `direc_solicitante`) VALUES
(1, 23724395, 'Andy Torres', 'Serv. Tecnico', '', ''),
(2, 23724441, 'Germain Rojas', 'Merida I', '', ''),
(3, 19894934, 'Andres Torres', 'MÃ©rida I', '04247844455', 'la parroquia'),
(4, 22657214, 'Yuberli Vielma', 'Servicio TÃ©cnico', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud`
--

CREATE TABLE `solicitud` (
  `cod_solicitud` int(11) NOT NULL,
  `fecha_solicitud` date NOT NULL,
  `hora_solicitud` time NOT NULL,
  `tipo_solicitud` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `cod_usuario` int(11) DEFAULT NULL,
  `cod_solicitante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `solicitud`
--

INSERT INTO `solicitud` (`cod_solicitud`, `fecha_solicitud`, `hora_solicitud`, `tipo_solicitud`, `cod_usuario`, `cod_solicitante`) VALUES
(1, '0000-00-00', '11:06:00', '', 36, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud_entrega`
--

CREATE TABLE `solicitud_entrega` (
  `cod_entrega` int(11) NOT NULL,
  `serial_material` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `nro_empresa` int(15) NOT NULL,
  `nro_orden` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `inst_referencia` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_actividad` text COLLATE utf8_spanish_ci NOT NULL,
  `cant_entrega` int(5) NOT NULL,
  `observacion` text COLLATE utf8_spanish_ci NOT NULL,
  `cod_solicitud` int(11) DEFAULT NULL,
  `cod_sap` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `solicitud_entrega`
--

INSERT INTO `solicitud_entrega` (`cod_entrega`, `serial_material`, `nro_empresa`, `nro_orden`, `inst_referencia`, `tipo_actividad`, `cant_entrega`, `observacion`, `cod_solicitud`, `cod_sap`) VALUES
(88, '123', 123, '123', 'ejido', 'AdecuaciÃ³n', 1, '', 1, 112233);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `cod_usuario` int(11) NOT NULL,
  `cod_usuario_bitacora` int(11) NOT NULL,
  `nomb_usuario` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `ced_usuario` int(12) NOT NULL,
  `tipo_usuario` enum('administrador','usuario','','') CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `passw_usuario` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `pregunta_seguridad` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `respuesta_seguridad` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`cod_usuario`, `cod_usuario_bitacora`, `nomb_usuario`, `ced_usuario`, `tipo_usuario`, `email`, `passw_usuario`, `pregunta_seguridad`, `respuesta_seguridad`) VALUES
(36, 0, 'andy', 44444444, 'administrador', 'andy@gmail.com', 'andy', '', ''),
(54, 36, 'gorda', 23874237, 'administrador', 'gorda@gmail.com', '311ffb7b8d804a5da6b8a6efd50f4ae5', 'GORDA', 'GORDA');

--
-- Disparadores `usuario`
--
DELIMITER $$
CREATE TRIGGER `insert_usuario` AFTER INSERT ON `usuario` FOR EACH ROW INSERT INTO bitacora SET cod_usuario = NEW.cod_usuario_bitacora , accion_bitacora = 'INSERTO', tabla_bitacora = 'USUARIO' , fecha_bitacora = NOW() , datos_bitacora = CONCAT( NEW.cod_usuario , '
' , NEW.nomb_usuario , '
', NEW.ced_usuario, '
', NEW.email)
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_usuario` AFTER UPDATE ON `usuario` FOR EACH ROW INSERT INTO bitacora SET cod_usuario = NEW.cod_usuario_bitacora , accion_bitacora = 'MODIFICO', tabla_bitacora = 'USUARIO' , fecha_bitacora = NOW() , datos_bitacora = CONCAT( OLD.cod_usuario , '
' , OLD.nomb_usuario , '
' ,OLD.ced_usuario, '
' , OLD.email)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_materiales`
--

CREATE TABLE `usuario_materiales` (
  `id_ingreso` int(11) NOT NULL,
  `cod_ingreso` int(11) NOT NULL,
  `cod_sap` int(11) NOT NULL,
  `cod_usuario` int(11) NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `cant_material` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario_materiales`
--

INSERT INTO `usuario_materiales` (`id_ingreso`, `cod_ingreso`, `cod_sap`, `cod_usuario`, `fecha_ingreso`, `cant_material`) VALUES
(1, 1, 112233, 36, '2016-10-04', 1),
(2, 1, 332211, 36, '2016-10-04', 1),
(3, 2, 112233, 36, '2016-10-04', 5),
(4, 2, 332211, 36, '2016-10-04', 5);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD PRIMARY KEY (`id_bitacora`);

--
-- Indices de la tabla `materiales`
--
ALTER TABLE `materiales`
  ADD PRIMARY KEY (`cod_sap`);

--
-- Indices de la tabla `solicitante`
--
ALTER TABLE `solicitante`
  ADD PRIMARY KEY (`cod_solicitante`);

--
-- Indices de la tabla `solicitud`
--
ALTER TABLE `solicitud`
  ADD PRIMARY KEY (`cod_solicitud`),
  ADD KEY `cod_solicitante` (`cod_solicitante`);

--
-- Indices de la tabla `solicitud_entrega`
--
ALTER TABLE `solicitud_entrega`
  ADD PRIMARY KEY (`cod_entrega`),
  ADD KEY `cod_solicitud` (`cod_solicitud`),
  ADD KEY `cod_sap` (`cod_sap`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`cod_usuario`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `ced_usuario` (`ced_usuario`);

--
-- Indices de la tabla `usuario_materiales`
--
ALTER TABLE `usuario_materiales`
  ADD PRIMARY KEY (`id_ingreso`),
  ADD KEY `cod_ingreso` (`cod_ingreso`),
  ADD KEY `cod_sap` (`cod_sap`,`cod_usuario`),
  ADD KEY `cod_usuario` (`cod_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  MODIFY `id_bitacora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `solicitante`
--
ALTER TABLE `solicitante`
  MODIFY `cod_solicitante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `solicitud_entrega`
--
ALTER TABLE `solicitud_entrega`
  MODIFY `cod_entrega` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `cod_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT de la tabla `usuario_materiales`
--
ALTER TABLE `usuario_materiales`
  MODIFY `id_ingreso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `solicitud`
--
ALTER TABLE `solicitud`
  ADD CONSTRAINT `solicitud_ibfk_2` FOREIGN KEY (`cod_solicitante`) REFERENCES `solicitante` (`cod_solicitante`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `solicitud_entrega`
--
ALTER TABLE `solicitud_entrega`
  ADD CONSTRAINT `solicitud_entrega_ibfk_1` FOREIGN KEY (`cod_solicitud`) REFERENCES `solicitud` (`cod_solicitud`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `solicitud_entrega_ibfk_2` FOREIGN KEY (`cod_sap`) REFERENCES `materiales` (`cod_sap`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario_materiales`
--
ALTER TABLE `usuario_materiales`
  ADD CONSTRAINT `usuario_materiales_ibfk_1` FOREIGN KEY (`cod_sap`) REFERENCES `materiales` (`cod_sap`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuario_materiales_ibfk_2` FOREIGN KEY (`cod_usuario`) REFERENCES `usuario` (`cod_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
