
  <body class="skin-blue">
    <div class="wrapper">
      
      <header class="main-header">
        <!-- Logo -->
        <a href="../index.php" class="logo"><b>CORPOELEC</b></a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <IMG SRC="../config/corpoelec3.png" width="480" height="90" ><IMG SRC="../config/corpoelec4.png" width="266" height="90" >
         
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="hidden-xs">
                    <br>
                    <?php
                      echo $_SESSION ["email"]. "  Conectado";
echo "<br>";
		      echo $_SESSION ["tipo_usuario"];
                     
?>
                    <br>                 
                  </span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <!-- Menu Footer-->
                  <li class="user-footer">                    
                    <div class="pull-left">
                      <a href="#" class="btn btn-xs btn-info">Perfil</a>
                    </div>
                    <div class="pull-right">
                      <a href="../usuario/cerrar.php" class="btn btn-xs btn-danger">Cerrar Sesion</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          
          
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Menu</li>
            <li>
              <a href="../index.php">
                <i class="fa fa-home"></i> <span>Inicio</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-cogs"></i>
                <span>Inventario</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="../materiales/materiales_cargar.php"><i class="fa fa-circle-o"></i> cargar Material</a></li>
                <li><a href="../solicitud_entrega/consultar_inventario.php"><i class="fa fa-circle-o"></i> Consultar Inventario</a></li>
                <li><a href="../materiales/consultar_ingreso.php"><i class="fa fa-circle-o"></i> Consultar Ingreso</a></li>

              </ul>
            </li>
            <li>
              <a href="../solicitud/solicitud_insertar.php">
                <i class="fa fa-th"></i> <span>Solicitudes</span> 
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Solicitantes</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="../solicitante/solicitante_index.php"><i class="fa fa-circle-o"></i> Listar</a></li>
                <li><a href="../solicitante/solicitante_insertar.php"><i class="fa fa-circle-o"></i> Insertar</a></li>
                
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>Entregas</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">


                <li><a href="../solicitud_entrega/solicitud_entrega_consultar.php"><i class="fa fa-circle-o"></i> Consultar</a>          


              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-gavel"></i>
                <span>Materiales</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="../materiales/materiales_insertar.php"><i class="fa fa-circle-o"></i> Ingresar  Material</a></li>
                <li><a href="../materiales/materiales_consultar.php"><i class="fa fa-circle-o"></i> Consultar Material</a></li>
                
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Usuario</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="../usuario/usuario.php"><i class="fa fa-circle-o"></i>Crear un Nuevo Usuario</a></li>
                <li><a href="../usuario/consul_usuario.php"><i class="fa fa-circle-o"></i> Consultar Usuario</a></li>
 <li><a href="../usuario/cerrar.php"><i class="fa fa-circle-o"></i>Cerrar Sesion</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-question"></i> <span>Ayuda</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="../ayuda/respaldo_bd.php"><i class="fa fa-gear"></i> Mantenimiento a la Base de datos</a></li>
                <li><a href="#"><i class="fa  fa-book"></i> Manual de Usuario</a></li>
              </ul>
            </li>
            
              
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            CORPOELEC
            <small>Sistema de Control de Inventario</small>
          </h1>
          
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">            
              <div class="col-lg-3 col-xs-6">
              <a href="../solicitud/solicitud_insertar.php">
                <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>*</h3>
                  <p>Procesar Solicitud</p>
                </div>
                <div class="icon">
                  <i class=" ion ion-clipboard"></i>
                </div>
                
              </div>
            </div><!-- ./col -->
            </a>
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <a href="../materiales/materiales_insertar.php"><div class="small-box bg-green">
                <div class="inner">
                  <h3>*</h3>
                  <p>Ingresar Material</p>
                </div>
                <div class="icon">
                  <i class="ion ion-briefcase"></i>
                </div>
              </div>
              </a>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <a href="../usuario/usuario.php"><div class="small-box bg-blue">
                <div class="inner">
                  <h3>*</h3>
                  <p>Registrar Usuario</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
              </div>
              </a>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <a href="../materiales/materiales_consultar.php"><div class="small-box bg-red">
                <div class="inner">
                  <h3>*</h3>
                  <p>Consultar Material</p>
                </div>
                <div class="icon">
                  <i class="ion ion-search"></i>
                </div>
              </div>
              </a>
            </div><!-- ./col -->
          </div>
        <!-- /.row -->
          <!-- Main row -->
          
